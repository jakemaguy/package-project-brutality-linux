FROM alpine:latest
RUN apk add --no-cache curl
RUN mkdir build && \
cd build && \
curl -L --output Project_Brutality-master.pk3 "https://github.com/pa1nki113r/Project_Brutality/archive/master.zip"
