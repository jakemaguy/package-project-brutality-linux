# Build Process

1. Download Gzdoom: https://zdoom.org/downloads
2. git clone https://gitlab.com/jakemaguy/package-project-brutality-linux.git
3. cd package-project-brutality-linux
4. ./run.sh

# Specify Optional Filepath for Project Brutality File
The ENV_VAR `OUT_DIR` can be set to tell the program where to put the archive:
`OUT_DIR=/mnt/c/Users/<USER_NAME>/Downloads ./run.sh`
This will put the project brutality on your windows partitions if you're using wsl.

`OUT_DIR` defaults to: `~/.config/gzdoom`, if not specified.

# Launching Gzdoom

[Gzdoom Command Line Arguments](https://zdoom.org/wiki/Command_line_parameters)

Use standard config file and project brutality - Prompts user to select IWAD

`gzdoom -config ~/.config/gzdoom/gzdoom.ini -file ~/.config/gzdoom/Project_Brutality-master.pk3`

Specify IWAD in command line args - In this example: DOOM2.wad

`gzdoom -iwad ~/.config/gzdoom/DOOM2.wad -config ~/.config/gzdoom/gzdoom.ini -file ~/.config/gzdoom/Project_Brutality-master.pk3`

Specify IWAD and WAD - scythe.wad as an example

`gzdoom -iwad ~/.config/gzdoom/DOOM2.wad -config ~/.config/gzdoom/gzdoom.ini -file ~/.config/gzdoom/Project_Brutality-master.pk3 ~/.config/gzdoom/scythe.wad`
