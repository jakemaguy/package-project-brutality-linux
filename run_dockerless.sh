#!/usr/bin/env bash

# OUT_DIR: Optional filepath to put the project brutality file
if [ -z ${OUT_DIR} ]; then
    OUT_DIR=~/.config/gzdoom
fi

#wget -O /tmp/Project_Brutality-master.pk3 https://github.com/pa1nki113r/Project_Brutality/archive/master.zip
curl -L -o /tmp/Project_Brutality-master.pk3 "https://github.com/pa1nki113r/Project_Brutality/archive/master.zip"
mv /tmp/Project_Brutality-master.pk3 ${OUT_DIR}

