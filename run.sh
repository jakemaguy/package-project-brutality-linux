#!/usr/bin/env bash

# OUT_DIR: Optional filepath to put the project brutality file
if [ -z ${OUT_DIR} ]; then
    OUT_DIR=~/.config/gzdoom
fi

docker build . -t project_brutality_pkger

docker run --rm --name project_brutality_pkger -t -d project_brutality_pkger
docker cp project_brutality_pkger:/build/Project_Brutality-master.pk3 ${OUT_DIR}
docker stop project_brutality_pkger
docker rmi project_brutality_pkger
